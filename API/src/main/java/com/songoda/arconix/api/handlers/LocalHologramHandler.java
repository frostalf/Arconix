package com.songoda.arconix.api.handlers;

import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.hologram.HologramObject;
import com.songoda.arconix.api.methods.serialize.Serialize;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by songoda on 4/2/2017.
 */
@SuppressWarnings("WeakerAccess")
public class LocalHologramHandler {
    private ArconixAPI api = ArconixAPI.getApi();

    public LocalHologramHandler() {
    }

    public List<HologramObject> getHologramList() {
        List<HologramObject> holograms = new ArrayList<>();
        if (api.hologramFile.getConfig().contains("Holograms")) {
            ConfigurationSection cs = api.hologramFile.getConfig().getConfigurationSection("Holograms");
            for (String key : cs.getKeys(false)) {
                List<String> list = api.hologramFile.getConfig().getStringList("Holograms." + key + ".lines");
                Location location = api.serialize().getInstance().unserializeLocation(api.hologramFile.getConfig().getString("Holograms." + key + ".location"));
                holograms.add(new HologramObject(key, location, new ArrayList<>(list)));
            }
        }
        return holograms;
    }

    public void stream(Chunk chunk) {/*
        if (chunk == null) return;
        for (HologramObject hologram : api.packetLibrary.getHologramManager().getHolograms()) {
            Location location = hologram.getLocation();
            if (location.getChunk().equals(chunk)) {
                api.packetLibrary.getHologramManager().addHologram(location);
            }
        } */
    }

    public void saveHologram(HologramObject hologram) {
        if (hologram.getName() == null || hologram.getWorld() == null || hologram.getLocation() == null) return;
        String serial = api.serialize().serializeLocation(hologram.getLocation());
        api.hologramFile.getConfig().set("Holograms." + hologram.getName() + ".location", serial);
        api.hologramFile.getConfig().set("Holograms." + hologram.getName() + ".lines", hologram.getLines());
    }
}