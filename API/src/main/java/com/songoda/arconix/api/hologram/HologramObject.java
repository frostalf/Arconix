package com.songoda.arconix.api.hologram;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.Collections;

public class HologramObject {

    private String name;

    private Location location;

    private ArrayList<String> lines;

    public HologramObject(String name, Location location, ArrayList<String> lines) {
        this.name = name;
        this.location = location;
        this.lines = lines;
    }

    public HologramObject(String name, Location location, String string) {
        this(name, location, new ArrayList<>(Collections.singletonList(string)));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location.clone();
    }

    public World getWorld() {
        return location.getWorld();
    }

    public double getX() {
        return location.getX();
    }

    public double getY() {
        return location.getY();
    }

    public double getZ() {
        return location.getZ();
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<String> getLines() {
        return new ArrayList<>(lines);
    }

    public void addLine(String string) {
        lines.add(string);
    }

    public void removeLine(int line) {
        lines.remove(line);
    }

    public void removeLine(String string) {
        lines.remove(string);
    }

    @Override
    public String toString() {
        return "Hologram:{"
                + "Name:\"" + name + "\","
                + "Location:{"
                + "World:\"" + location.getWorld().getName() + "\","
                + "X:" + location.getBlockX() + ","
                + "Y:" + location.getBlockY() + ","
                + "Z:" + location.getBlockZ()
                + "}";
    }
}
