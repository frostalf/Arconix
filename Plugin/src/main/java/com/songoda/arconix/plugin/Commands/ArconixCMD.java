package com.songoda.arconix.plugin.Commands;

import com.songoda.arconix.api.methods.Formatting;
import com.songoda.arconix.plugin.Arconix;
import com.songoda.arconix.plugin.Commands.Subcommands.*;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ArconixCMD extends BaseCommand {

    private final Map<String, SubCommand> children = new HashMap<>();

    private Formatting formatting = new Formatting();

    public ArconixCMD() {

        super("Arconix", "ArconixCMD.command");

        children.put("title", new TitleSubCMD());
        children.put("subtitle", new SubtitleCMD());
        children.put("actionbar", new ActionBarCMD());
        children.put("hologram", new HologramCMD());
        children.put("reload", new ReloadCMD());
        children.put("signeditor", new SignEditorCMD());
        children.put("region", new RegionCMD());
        children.put("ping", new PingCMD());
    }

    @Override
    public void execute(Player p, String[] args) {

        if (args.length == 0) {
            p.sendMessage("");
            p.sendMessage(formatting.formatText("&8[&6Arconix&8] &7Version " + Arconix.pl().getDescription().getVersion() + " Created with <3 by &5&l&oBrianna"));
            p.sendMessage(formatting.formatText("&8 - &aArconix title/subtitle [FadeIn/Stay/FadeOut] <msg>"));
            p.sendMessage(formatting.formatText("&8 - &aArconix actionbar <msg>"));
            p.sendMessage(formatting.formatText("&8 - &aArconix hologram &7- Has Subcommands"));
            p.sendMessage(formatting.formatText("&8 - &aArconix reload"));
            p.sendMessage(formatting.formatText("&8 - &aArconix signeditor"));
            p.sendMessage(formatting.formatText("&8 - &aArconix region &7- Has Subcommands"));
            p.sendMessage(formatting.formatText("&8 - &aArconix ping"));
            p.sendMessage("");
            return;
        }

        SubCommand child = children.get(args[0].toLowerCase());

        if (child != null) {
            child.execute(p, args);
        }
    }

    public boolean registerChildren(SubCommand command) {

        if (children.get(command.getName()) != null) {
            return false;
        }

        children.put(command.getName(), command);
        return true;
    }
}
